#!/bin/bash

yq -o=props contract.yml| tr -d "[:blank:]" > contract.env

for i in 0 1 # 2 
do

## remove all lines except $i
AKS=$(sed "/^azure.0.aks.${i}/!d" contract.env)

## add space and remove newline | remove yaml and add --
ALLFLAGS=$(echo $AKS | sed "s/azure.0.aks.${i}./--/g")

echo "az create aks $ALLFLAGS"

done
